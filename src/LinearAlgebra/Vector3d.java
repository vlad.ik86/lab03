//Vladyslav Berezhnyak (1934443)

package LinearAlgebra;

public class Vector3d 
{
	private double x;
	private double y;
	private double z;
	
	public Vector3d(double inpX, double inpY, double inpZ)
	{
		this.x = inpX;
		this.y = inpY;
		this.z = inpZ;
	}

	public double getX() 
	{
		return this.x;
	}
	
	public double getY()
	{
		return this.y;
	}
	
	public double getZ()
	{
		return this.z;
	}
	
	public double magnitude()
	{
		double formula = Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2));
		return formula;
	}
	
	public double dotProduct(double inpX, double inpY, double inpZ)
	{
		double formula = (this.x * inpX) + (this.y * inpY) + (this.z * inpZ);
		return formula;
	}
	
	public Vector3d add(double inpX, double inpY, double inpZ)
	{
		double formX = this.x + inpX;
		double formY = this.y + inpY;
		double formZ = this.z + inpZ;
		
		Vector3d res = new Vector3d(formX, formY, formZ);
		return res;
	}
}
