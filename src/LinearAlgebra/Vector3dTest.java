//Vladyslav Berezhnyak (1934443)

package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTest 
{

	/*
	@Test
	void test() 
	{
		fail("Not yet implemented");
	}
	*/
	
	@Test
	void testCreateVector()
	{
		Vector3d v = new Vector3d(3.0, 2.0, 4.0);
		
		assertEquals(v.getX(), 3.0);
		assertEquals(v.getY(), 2.0);
		assertEquals(v.getZ(), 4.0);
	}
	
	@Test
	void testVectorMagnitude()
	{
		Vector3d v = new Vector3d(3.0, 2.0, 4.0);
		
		assertEquals(Math.round(v.magnitude()), 5.0);
	}
	
	@Test
	void testVectorDotProduct()
	{
		Vector3d v1 = new Vector3d(3.0, 2.0, 4.0);
		Vector3d v2 = new Vector3d(6.0, 1.0, 5.0);
		
		assertEquals(v1.dotProduct(v2.getX(), v2.getY(), v2.getZ()), 40);
	}
	
	@Test
	void testVectorAdd()
	{
		Vector3d v1 = new Vector3d(3.0, 2.0, 4.0);
		Vector3d v2 = new Vector3d(6.0, 1.0, 5.0);
		Vector3d v3 = v1.add(v2.getX(), v2.getY(), v2.getZ());
		
		assertEquals(v3.getX(), 9.0);
		assertEquals(v3.getY(), 3.0);
		assertEquals(v3.getZ(), 9.0);
	}
}
